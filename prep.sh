#!/bin/sh

# this script will do all of the necessary prep to get things going for the application
# pull in libs etc, or maybe even some kind of portable deployment, thusly not relying on
# stuff being installed in the usual way...
# tbh i'm writing this as node is still building on my netbook. at the same time as gnuradio
# that was probably a mistake in terms of productivity tonight
# although gnunradio is cool, if you build it with the buildscript off the site and then build
# gqrx - plug in an RTL sdr dongle - you can pick up all kinds of interesting stuff
# ... i digress

rootDirectory=${PWD}

cd html
rm -r inc
mkdir inc
cd inc
wget http://code.jquery.com/jquery-2.0.3.min.js
wget https://github.com/twbs/bootstrap/releases/download/v3.0.3/bootstrap-3.0.3-dist.zip
unzip bootstrap-3.0.3-dist.zip
rm bootstrap-3.0.3-dist.zip
mv dist bootstrap

cd $rootDirectory

npm install connect sqlite3
