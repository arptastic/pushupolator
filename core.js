var connect = require('connect'),
	staticDirectory = connect.static('html'),
	server = connect.createServer(staticDirectory);

server.listen(8080);
