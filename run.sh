#!/bin/sh

node core.js &
node_pid=$!

echo "Node PID: $node_pid"

# wait for input
read -p "Press enter to end run..." x

kill $node_pid
wait

echo "Done"
